from jose import jwt, JWTError
from schemas import Token
from fastapi import HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from config import settings


SERVER_KEY = settings.secret_key
ALGORITHM = settings.algorithm

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth")

def generate_token(user_id: int):
    payload = {"user_id": user_id}
    encoded_jwt = jwt.encode(payload, SERVER_KEY, ALGORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer")

def decode_token(provided_token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, ALGORITHM)
        decoded_id: str = payload.get("user_id")
    except JWTError:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, "Invalid Token", headers={"WWW-Authenticate": "Bearer"})
    return decoded_id