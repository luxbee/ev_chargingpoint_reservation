import requests
from typing import List
import psycopg2
from config import settings
from psycopg2.extras import RealDictCursor
from pydantic import datetime_parse
from datetime import datetime
import models

try:
    connection = psycopg2.connect(
        host=settings.database_host,
        database=settings.database_name,
        user=settings.database_username,
        password=settings.database_password,
        cursor_factory=RealDictCursor
    )
    cursor = connection.cursor()
except Exception as error:
    print('db connect failled')
    print('error :', error)

def update_notavailable_connector():
    sql_update_connector = """update connector set status='Available' where exists
(select 1 from reservations
where reservations."connector_id" = connector."id" and reservations."endTime" < now())"""
    sql_delete_reservations = """delete from reservations where reservations."endTime" < now()"""
    cursor.execute(sql_update_connector)
    cursor.execute(sql_delete_reservations)
    connection.commit()
    

class ChargingPoints():
    
#retrieve charging point and connector form api.edrv.io for the register user corresponding Token and insert it in the DB

    def get_charging_points(self):
        params = {}
        #chargingPoints=[]
        head={"accept": "application/json", 
              "authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjRWV1BWS3dveS1CMkl3Y2t3YlNERCJ9.eyJodHRwczovL2FwaS5lZHJ2LmlvL2NsaWVudF9tZXRhZGF0YSI6eyJhcHBsaWNhdGlvbiI6IjYzOTcxNDJhZGNjMWViMGVjNzgxZGJhMyJ9LCJpc3MiOiJodHRwczovL2F1dGguZWRydi5pby8iLCJzdWIiOiJQU0wyZndEUGhTdGM5dmZTVmZvZEVUZzQxc3pKanhuMkBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9hcGkuZWRydi5pbyIsImlhdCI6MTY3MDg0NTQ4NCwiZXhwIjoxNjcxNzA5NDg0LCJhenAiOiJQU0wyZndEUGhTdGM5dmZTVmZvZEVUZzQxc3pKanhuMiIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyIsInBlcm1pc3Npb25zIjpbXX0.EsPXQ2lqI9AUTMIHJrUMnVRBieQL9zPOhXulXBWR1YymYpMopaI-Q5sFUZhyZWNE2sXMyFXRWzzz17PTgcBjPuisb697iTyHZyMp3Mh8od9Gz-gHMAnLPmm9Jv_9hRHwbauEb98fEiH7ppDe-eDtrBi8Mp-U_5PXW9_G6nW6USnJ_t8GH1PnO9LRALNsWi_4p1QrkyPjnWRedYXOHUPcgCMCVPD6ZduGObqt1vbdCXc1-z0Rddw7TAtgwGFoDvyQn4relZ5S5v5W66DQo4xXIHvLly-uxdpeQg4CCiLyQYKp5LUTqLsOhF3GnsD39WmmLoXSqV3mVMLyimCzCFBIjA"}
        location_req = requests.get('https://api.edrv.io/v1.1/locations?paginate_limit=20&paginate_enabled=true&sort_by=createdAt&sort_order=desc', headers=head)
        charging_point_req = requests.get('https://api.edrv.io/v1.1/chargestations?paginate_limit=20&paginate_enabled=true&sort_by=createdAt&sort_order=desc', headers=head)
        location_resp= location_req.json()
        chargin_point_resp = charging_point_req.json()
        print(location_resp)
        for location in location_resp["result"]:
            new_location = models.Location(id=location["_id"],
                                           streetandnumber=location["address"]["streetAndNumber"],
                                           city=location["address"]["city"],
                                           country=location["address"]["country"],
                                           postalcode=location["address"]["postalCode"],
                                           state=location["address"]["state"],
                                           timezone=location["timezone"],
                                           latitude=location["coordinates"][1],
                                           longitude=location["coordinates"][0]
                                           )
            cursor.execute("insert INTO location (id, streetandnumber, city, country, postalcode, state, timezone, latitude, longitude) values (%s, %s, %s, %s, %s, %s, %s, %s, %s);",
                           (new_location.id, new_location.streetandnumber, new_location.city, new_location.country, new_location.postalcode, new_location.state, 
                            new_location.timezone, new_location.latitude, new_location.longitude))
            
        #chargin_point_resp = charging_point_req.json()
        
        for chargingPoint in chargin_point_resp["result"]:
            new_charging_point = models.ChargingPoint(location_id=chargingPoint["location"],
                                                      protocol=chargingPoint["protocol"],
                                                      active=chargingPoint["active"],
                                                      public=chargingPoint["public"],
                                                      connection_url=chargingPoint["connection_url"]
                                                      )
            #chargingPoints.append(new_charging_point)
            cursor.execute("insert INTO charging_point (location_id, protocol, active, public, connection_url) values (%s, %s, %s, %s, %s) RETURNING id;",
                           (new_charging_point.location_id, new_charging_point.protocol, new_charging_point.active, new_charging_point.public,
                           new_charging_point.connection_url))
            id_new_charging_point = cursor.fetchone()
            #print(id_new_charging_point["id"])
            for connector in chargingPoint["connectors"]:
                new_connector = models.Connector(active=connector["active"],
                                                 status=connector["status"],
                                                 type=connector["type"],
                                                 format=connector["format"],
                                                 power_type=connector["power_type"],
                                                 charging_point_id=id_new_charging_point["id"])
                cursor.execute("insert INTO connector (active, status, type, format, power_type, charging_point_id) values (%s, %s, %s, %s, %s, %s);",
                               (new_connector.active, new_connector.status, new_connector.type, new_connector.format, new_connector.power_type, new_connector.charging_point_id))
            
        connection.commit()