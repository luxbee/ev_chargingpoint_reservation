from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, ForeignKeyConstraint, NUMERIC, DATETIME
from sqlalchemy.sql.sqltypes import TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.orm import relationship

Base = declarative_base()

class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, nullable=False)
    email = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False)
    
class Location(Base):
    __tablename__= "location"
    id = Column(String, primary_key=True, nullable=False)
    streetandnumber = Column(String, nullable=True, unique=False)
    city = Column(String, nullable=True, unique=False)
    country = Column(String, nullable=True, unique=False)
    postalcode = Column(String, nullable=True, unique=False)
    state = Column(String, nullable=True, unique=False)
    timezone = Column(String, nullable=True, unique=False)
    latitude = Column(String, nullable=True, unique=False)
    longitude = Column(String, nullable=True, unique=False)
    createdAt = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False)
    

class ChargingPoint(Base):
    __tablename__= "charging_point"
    id = Column(Integer, primary_key=True, nullable=False)
    location_id = Column(String, ForeignKey("location.id"), nullable=False, unique=False)
    protocol = Column(String, nullable=True, unique=False)
    active = Column(Boolean, nullable=True, unique=False)
    public = Column(Boolean, nullable=True, unique=False)
    connection_url = Column(String, nullable=True, unique=False)
    note = Column(NUMERIC, nullable=True, unique=False)
    createdAt = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False)
    location = relationship(Location)
    connectors = relationship("Connector", back_populates="charging_point")
    
class Connector(Base):
    __tablename__= "connector"
    id = Column(Integer, primary_key=True, nullable=False)
    active = Column(Boolean, nullable=True, unique=False)
    status = Column(String, nullable=True, unique=False)
    type = Column(String, nullable=True, unique=False)
    format = Column(String, nullable=True, unique=False)
    power_type = Column(String, nullable=True, unique=False)
    charging_point_id = Column(Integer, ForeignKey("charging_point.id"), nullable=False, unique=False)
    charging_point = relationship("ChargingPoint", back_populates="connectors")

class Reservation(Base):
    __tablename__ = "reservations"
    id = Column(Integer, primary_key=True, nullable=False)
    connector_id = Column(Integer, ForeignKey("connector.id"), nullable=False, unique=False)
    connector = relationship("Connector")
    startTime = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False)
    endTime = Column(TIMESTAMP(timezone=True), server_default=text("now() + interval '2 hour'"), nullable=False)
    createdAt = Column(TIMESTAMP(timezone=True), server_default=text('now()'), nullable=False)