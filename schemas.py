from datetime import datetime
from pydantic import BaseModel, EmailStr
from typing import List, Union, Optional, ForwardRef

class UserPy(BaseModel):
    email: EmailStr
    password: str

class User_Credentials(UserPy):
    pass

class User_Response(UserPy):
    id: int
    created_at: datetime
    class Config:
        orm_mode = True
        
class Token(BaseModel):
    access_token: str
    token_type: str
    
class ConnectorPy(BaseModel):
    active: bool
    status: str
    type: str
    format: str
    power_type : str
    charging_point_id : int


ChargingPoint_Response = ForwardRef('ChargingPoint_Response')
    
class Connector_Response(ConnectorPy):
    #charging_point: ChargingPoint_Response
    id: int
    class Config:
        orm_mode = True

class LocationPy(BaseModel):
    id: str
    streetandnumber: str
    city: str
    country: str
    postalcode: str
    state: str
    timezone: str
    latitude: str
    longitude: str

class Location_Response(LocationPy):
    createdAt: datetime
    class Config:
        orm_mode = True

class ChargingPointPy(BaseModel):
    protocol: str
    active: bool
    public: bool
    connection_url: str
    note: Optional[str] = None
    location_id: str
        
class ChargingPoint_Response(ChargingPointPy):
    id: int
    createdAt: datetime
    location: Location_Response
    connectors: List[Connector_Response] = []
    class Config:
        orm_mode = True
    
ChargingPoint_Response.update_forward_refs()

class ReservationPy(BaseModel):
    connector_id: int

class Reservation_Response(ReservationPy):
    id: int
    startTime: datetime
    endTime: datetime
    createdAt: datetime
    connector: Connector_Response
    class Config:
        orm_mode = True
    
    
#Connector_Response.update_forward_refs()

#class Connector_Response(ConnectorPy):
#    id: int
#    charging_point: Cha
    

    
    