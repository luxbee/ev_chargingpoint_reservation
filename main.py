from data.charging_point import ChargingPoints, update_notavailable_connector
import models
from database import get_db, databas_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
import models, schemas
from utilities import hash_manager, jwt_manager
from sqlalchemy.orm import Session
import uvicorn
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from typing import List
from sqlalchemy import update
from threading import Thread
from time import sleep

#models.Base.metadata.drop_all(bind=databas_engine)
models.Base.metadata.create_all(bind=databas_engine)

#just execute one time to populate tables with api consumption than comment

#my_charging_point = ChargingPoints()
#my_charging_point.get_charging_points()

def delete_old_reservation():
    while True:
        update_notavailable_connector()
        sleep(5)

    
app = FastAPI()


@app.post('/users', response_model=schemas.User_Response)
def create_user(user_body:schemas.UserPy, db:Session=Depends(get_db)):
    pwd_hashed = hash_manager.hash_pass(user_body.password)
    user_body.password=pwd_hashed
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

incorrectExcept = HTTPException(status.HTTP_401_UNAUTHORIZED, "Incorrect username or password", headers={"WWW-Authenticate": "Bearer"})

@app.post('/auth', response_model=schemas.Token)
def auth_user(user_credentials: OAuth2PasswordRequestForm = Depends(), db:Session=Depends(get_db)):
    corresponding_user : models.User = db.query(models.User).filter(models.User.email == user_credentials.username).first()
    if not corresponding_user:
        raise incorrectExcept
    pass_valid = hash_manager.verify_password(user_credentials.password, corresponding_user.password)
    if not pass_valid:
        raise incorrectExcept
    jwt = jwt_manager.generate_token(corresponding_user.id)
    return jwt

@app.get('/users/me', response_model=schemas.User_Response)
def get_user_me(user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    corresponding_user = db.query(models.User).filter(models.User.id == user_id).first()
    return corresponding_user

@app.get('/users', response_model=List[schemas.User_Response])
def get_users(user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    users = db.query(models.User).all()
    return users

@app.get('/locations', response_model=List[schemas.Location_Response])
def get_locations(user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    locations = db.query(models.Location).all()
    return locations

#@app.get('/chargingPoints', response_model=List[schemas.ChargingPoint_Response])
@app.get('/chargingPoints', response_model=List[schemas.ChargingPoint_Response])
def get_chargingPoints(user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    chargingPoints= db.query(models.ChargingPoint).all()
    return chargingPoints

@app.get('/connectors', response_model=List[schemas.Connector_Response])
def get_connectors(user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    connectors= db.query(models.Connector).all()
    return connectors

@app.put('/connector/{connector_id}', response_model=schemas.Connector_Response)
def udpdate_connector(connector_id: int, connector_body:schemas.ConnectorPy, 
                      user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    connector=db.query(models.Connector).filter(models.Connector.id == connector_id)
    corresponding_connector=connector.first()
    if not corresponding_connector:  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding connector was found with id:{connector_id}"
        )
    connector.update(connector_body.dict())
    db.commit()
    db.refresh(connector)
    return connector.first()

@app.post('/reservation/{connector_id}', response_model=schemas.Reservation_Response)
def make_reservation(connector_id: int, user_id:int = Depends(jwt_manager.decode_token), db: Session = Depends(get_db)):
    connector=db.query(models.Connector).filter(models.Connector.id == connector_id)
    corresponding_connector=connector.first()
    if not corresponding_connector:  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding connector was found with id:{connector_id}"
        )
    if corresponding_connector.status != 'Available':
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Corresponding connector is not Available:{connector_id}"
        )
    connector.update({"status":"Not Available"})
    #db.execute(
    #    update(models.Connector),
    #    [{"id": connector_id, "status":"Not Available"}]
    #)
    new_reservation = models.Reservation(connector_id=connector_id)
    db.add(new_reservation)
    db.commit()
    db.refresh(new_reservation)
    db.refresh(corresponding_connector)
    return new_reservation
    
    

#comment/uncomment to start uvicorn
if __name__ == "__main__":
    thread = Thread(target=delete_old_reservation)
    thread.setDaemon(True)
    thread.start()
    #other_thread=Thread(target=uvicorn.run("main:app", port=8000, log_level="info", reload="True"))
    uvicorn.run("main:app", port=8000, log_level="info", reload="True")
    #other_thread.start()
    