from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import settings

#DATABASE_URL='postgresql://postgres:root@localhost:5432/sqlalchemy'

DATABASE_URL=f'postgresql://{settings.database_username}:{settings.database_password}@{settings.database_host}:{settings.database_port}/{settings.database_name}'

databas_engine = create_engine(DATABASE_URL)

SessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=databas_engine)

def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()